const { default: migrate } = require('node-pg-migrate');

const {
  DB_USER,
  DB_PASS,
  DB_NAME,
  DB_HOST,
  DB_PORT,
} = process.env;

console.log(process.env)

module.exports = async () => {
  const options = {
    databaseUrl: {
      host: DB_HOST,
      port: DB_PORT,
      user: DB_USER,
      password: DB_PASS,
      database: DB_NAME,
    },
    migrationsTable: 'pgmigrations',
    dir: 'migrations',
    direction: 'up',
    count: Infinity,
  };

  try {
    await migrate(options);
  } catch (e) {
    console.error('DB migration has failed', e);
    process.exit(1);
  }
};
