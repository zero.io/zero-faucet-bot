require('dotenv').config();
const dbMigration = require('./db/migration');
const client = require('./modules/client');

if (!process.env.MNEMONIC) {
  console.error(`The environment variable 'mnemonic' was not set.`);
  process.exit();
}

if (!process.env.NODE_URL) {
  console.error(`The environment variable 'nodeUrl' was not set.`);
  process.exit();
}

if (!process.env.ADDRESS_FORMATS) {
  console.error(`The environment variable 'formats' was not set.`);
  process.exit();
}

if (!process.env.VERIFY_ROLE) {
  console.error(`The environment variable 'VERIFY_ROLE' was not set.`);
  process.exit();
}

dbMigration();
client();
