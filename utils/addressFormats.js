const formats = process.env.ADDRESS_FORMATS.split(',').map((x) => +x);

module.exports = formats;
