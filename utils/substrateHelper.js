const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api');
const crypto = require('@polkadot/util-crypto');
const { BN } = require('bn.js');
const types = require('./types.json');

module.exports = {
  /**
   * Get Sender account from mnemonic
   */
  getSender: () => {
    return new Keyring({ type: 'sr25519' }).addFromMnemonic(
      process.env.MNEMONIC,
    );
  },

  /**
   * Connect to node and retun an new api instance
   */
  connectToNode: async () => {
    const provider = new WsProvider(process.env.NODE_URL);
    const api = await ApiPromise.create({
      provider,
      types
    });

    return api.isConnected ? api : null;
  },

  /**
   * Format balance
   */
  toUnit: (balance, decimals) => {
    base = new BN(10).pow(new BN(decimals));
    dm = new BN(balance).divmod(base);
    return parseFloat(dm.div.toString() + '.' + dm.mod.toString());
  },

  /**
   * Check address
   */
  checkAddress: (address, prefix = 42) => {
    if (!crypto.checkAddress(address, prefix)?.[0]) {
      return new Error(
        'The address format you have entered does not work here. Please use one of the supported formats, ZERO, Alphaville, Substrate, Polkadot or Kusama.',
      );
    }
    return false;
  },
};
