const substrateHelper = require('../utils/substrateHelper');
const addressFormats = require('../utils/addressFormats');
const { to } = require('await-to-js');

module.exports = {
  /**
   * Transfer to address
   *
   * @param {String} address
   * @param {import('bn.js').BN} amount
   *
   * @returns Error || HexString
   */
  send: async (address, amount) => {
    const checks = addressFormats.map((format) =>
      substrateHelper.checkAddress(address, format),
    );

    if (checks.filter((x) => x === false).length === 0) {
      return new Error(
        'The address you entered is wrong. Please use one of the supported formats [Zero, Substrate, Polkadot Relay Chain, Kusama Relay Chain]',
      );
    }

    const api = await substrateHelper.connectToNode();
    if (!api) {
      return new Error('Sorry, I could not connect zero network.');
    }

    const [err, hash] = await to(
      api.tx.balances
        .transferKeepAlive(address, amount)
        .signAndSend(substrateHelper.getSender()),
    );

    api.disconnect();

    if (err) {
      console.error(err);
      return new Error(
        'I could not send you anything, because an unknown error occurred.',
      );
    }

    console.log(`Transfered ${amount} to ${address} with hash ${hash.toHex()}`);
    return hash.toHex();
  },
};
