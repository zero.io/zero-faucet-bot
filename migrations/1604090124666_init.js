/* eslint-disable camelcase */
require('node-pg-migrate');
exports.shorthands = undefined;

/**
 * @param {import('node-pg-migrate')} pgm
 */
exports.up = (pgm) => {
  pgm.createTable('users', {
    id: 'id',
    user_id: { type: 'varchar(32)', notNull: true },
    last_coins: {
      type: 'varchar(32)',
      notNull: true,
    },
  });

  pgm.createTable('statics', {
    id: 'id',
    user_id: { type: 'varchar(32)', notNull: true },
    use_count: { type: 'integer' },
  });
};

exports.down = (pgm) => {};
