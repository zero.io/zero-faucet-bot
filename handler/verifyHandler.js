const substrateHelper = require('../utils/substrateHelper');
const addressFormats = require('../utils/addressFormats');

/**
 * Verify messages are processed here
 *
 * @param {import('discord.js').Message} msg
 * @param {Array<String>} args
 */
module.exports = async (msg, args) => {
  if (!args.length) {
    return msg.reply('Insufficient parameters were specified');
  }

  // Check if the role exists on the server.
  if (
    (
      msg?.guild?.roles?.cache?.filter(
        (role) => role.name === process.env.VERIFY_ROLE,
      ) || new Map()
    ).size === 0
  ) {
    console.warn('Either no server or there is no role verified on the server');
    return msg.reply(
      'An error has occurred in the configuration of the server.',
    );
  }

  // Check if the user does not already have the role
  const guildMember = msg.guild.member(msg.author);
  if (
    (
      guildMember?.roles?.cache?.filter(
        (role) => role.name === process.env.VERIFY_ROLE,
      ) || new Map()
    ).size !== 0
  ) {
    return msg.reply('Your account is already verified.');
  }

  // Check if the address format is supported
  const address = args[0];
  const checks = addressFormats.map((format) =>
    substrateHelper.checkAddress(address, format),
  );

  if (checks.filter((x) => x === false).length === 0) {
    return new Error(
      'The address you entered is wrong. Please use one of the supported formats [Zero, Substrate, Polkadot Relay Chain, Kusama Relay Chain]',
    );
  }

  // Create api Connection
  const api = await substrateHelper.connectToNode();
  if (!api) {
    console.error('I could not connect to the substrate node.');
    return msg.reply('I could not connect to the substrate node.');
  }

  // Get the user identity and check if the discord user has entered his Discord-ID
  let hasValidId = false;
  const identity = await api.query.identity.identityOf(address);
  identity?.value?.info?.additional?.forEach((items) => {
    const field = Buffer.from(items?.[0]?.asRaw || '')
      .toString()
      .toLowerCase();
    if (field === 'discord') {
      if (
        msg.author.id.toLowerCase() ===
        Buffer.from(items?.[1]?.asRaw || '')
          .toString()
          .toLowerCase()
      ) {
        hasValidId = true;
      }
    }
  });

  // Close api Connection
  api.disconnect();

  if (hasValidId) {
    // Get the server role and give it to the user
    guildMember.roles.add(
      msg.guild.roles.cache.find(
        (role) => role.name === process.env.VERIFY_ROLE,
      ),
    );
    return msg.reply('Your account was successfully verified.');
  }

  return msg.reply('Your account could not be verified.');
};
