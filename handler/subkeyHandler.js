const substrateHelper = require('../utils/substrateHelper');
const addressFormats = require('../utils/addressFormats');
const spawn = require('await-spawn');

/**
 * Subkey messages are processed here
 *
 * @param {import('discord.js').Message} msg
 * @param {Array<String>} args
 */
module.exports = async (msg, args) => {
  if (!args.length) {
    return msg.reply('Insufficient parameters were specified');
  }

  const address = args[0];

  const checks = addressFormats.map((format) =>
    substrateHelper.checkAddress(address, format),
  );

  if (checks.filter((x) => x === false).length === 0) {
    return msg.reply(
      'The address you entered is wrong. Please use one of the supported formats [Zero, Substrate, Polkadot Relay Chain, Kusama Relay Chain]',
    );
  }

  let keySR;
  let keyED;
  try {
    keySR = await spawn('subkey', ['inspect', '--scheme', 'sr25519', address]);
    keyED = await spawn('subkey', ['inspect', '--scheme', 'ed25519', address]);
  } catch (e) {
    console.error('Subkey tool error', e);
    msg.reply('There was an error when calling the subkey tool 😢');
  }

  msg.reply(
    'I have found the following information for you \r\n' +
      'For scheme `sr25519`:\r\n' +
      '```' +
      keySR.toString() +
      '``` \r\n' +
      'For scheme `ed25519`:\r\n' +
      '```' +
      keyED.toString() +
      '``` \r\n',
  );
};
